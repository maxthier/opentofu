#!/usr/bin/env bash

set -o errexit
set -o errtrace

if [ -z "$1" ]; then
  echo "Error: please provide a version for this release as the first argument." >&2
  exit 1
fi

if [ -z "${GITLAB_TOKEN}" ]; then
  echo "Error: please set the GITLAB_TOKEN environment variable." >&2
  exit 1
fi

if [ "main" != "$(git rev-parse --abbrev-ref HEAD)" ]; then
  echo "Error: please checkout the main branch first: git checkout main." >&2
  exit 1
fi

script_dir=$(dirname "$0")
version="$1"

echo "Verifying release version '${version}' ..."
echo "${version}" | "${script_dir}/check-semantic-version.sh"

echo "Starting release process for ${version} ..."

echo "Determing last stable version ..."
last_stable_version_sha="$(git tag | grep -E '^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$' | sort --version-sort --reverse | head -n1 | xargs git rev-list -n1)"
echo "Using commit '${last_stable_version_sha}' to start the changelog from ..."

echo "Creating changelog ..."
curl \
  --verbose \
  --fail-with-body \
  --request POST \
  --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  --header 'Content-Type: application/json' \
  'https://gitlab.com/api/v4/projects/components%2Fopentofu/repository/changelog' \
  --data "
  {
    \"version\": \"${version}\",
    \"from\": \"${last_stable_version_sha}\",
    \"message\": \"Add changelog for ${version}\"
  }"

echo "Pulling changelog ..."
git pull

echo "Tagging ${version} ..."
git tag "${version}"

echo "Pushing tag ${version} ..."

git push origin "${version}"

echo "Created tag ${version}, pipeline triggered, release will be available soon!"
