#!/usr/bin/env sh

set -e

# This script is just used to add the `latest_version` to the
# list of supported versions because natively that is
# cumbersome to achieve.

script_dir="$(dirname "$0")"
project_dir="$script_dir/../.."

yq --inplace '.".data".supported_versions = ([.".data".latest_version] + .".data".supported_versions | unique)' "$project_dir/opentofu_versions.yaml"

"$script_dir/update-opentofu-versions.sh"

make -C "$project_dir"
