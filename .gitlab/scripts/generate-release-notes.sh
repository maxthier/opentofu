#!/usr/bin/env sh

set -e

# shellcheck disable=SC2016
AVAILABLE_OPENTOFU_VERSIONS="$(yq -r '.spec.inputs.opentofu_version.options | filter((. | test("\$.*")) == false) | .[] | "- [`" + . + "`](https://github.com/opentofu/opentofu/releases/tag/v" + . + ")"' templates/full-pipeline.yml)"
export AVAILABLE_OPENTOFU_VERSIONS

AVAILABLE_IMAGES="$(cat image*.md | sort -r | tee images.md | sed -E "s/(\(digest: .*\))/\n  - \1/")"
export AVAILABLE_IMAGES

CHANGELOG="$(awk '/^## /{i++} i==1 && NR>1' CHANGELOG.md)"
export CHANGELOG

envsubst < .gitlab/release-notes.md.template
